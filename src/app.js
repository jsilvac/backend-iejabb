const morgan = require("morgan");
const mongoose = require("mongoose");
const express = require("express");
const cookieParser = require("cookie-parser");
const path = require("path");
const bodyParser = require("body-parser");

require("dotenv").config();

const indexRoutes = require("./routes/index");

//Declaración de servidor.
const server = express();

//---- Autenticación
server.use(cookieParser());

server.use(bodyParser.json());

server.use(bodyParser.urlencoded({ extended: true }));

//--------------------------------------

server.use("/public", express.static(path.join(__dirname, "public")));

server.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  res.header("Access-Control-Allow-Methods", "*");
  next();
});

// TODO: connection to db
mongoose
  .connect("mongodb://localhost:27017/database", {
    useNewUrlParser: true,
    useCreateIndex: true
  })
  .then(db => console.log("Connected to DB successfully !!!"))
  .catch(err => console.log(err));

// settings
server.set("port");

//Middlewares.
server.use(morgan("dev"));

// when user send data it is formated in json
// extended false it is becuase the data is not image or large files
// it is only for text and short text

server.use("/public", express.static("public"));

server.use(express.urlencoded({ extended: true }));

// routes
server.use("/", indexRoutes);

server.listen(process.env.PORT || 4000, () => {
  console.log(`server on port ${process.env.PORT || 4000}`);
});
