const mongoose = require("mongoose");
const bcrypt = require("bcrypt");

const userSchema = new mongoose.Schema(
  {
    username: {
      type: String,
      trim: true,
      unique: true,
      dropDups: true,
      required: [true, "can't be blank"],
      match: [/^[a-zA-Z0-9]+$/, "is invalid"]
    },
    full_name: {
      type: String,
      trim: true,
      unique: true,
      dropDups: true,
      required: [true, "can't be blank"],
      match: [/^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/, "is invalid"]
    },
    email: {
      type: String,
      lowercase: true,
      unique: true,
      dropDups: true,
      required: [true, "can't be blank"],
      match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, "is invalid"],
      trim: true
    },
    hash_password: {
      type: String,
      required: true
    }
  },
  { timestamps: true }
);

userSchema.index({ full_name: "text", username: "text" });
userSchema.methods.comparePassword = function comparePassword(password) {
  return bcrypt.compareSync(password, this.hash_password);
};

const User = mongoose.model("user", userSchema);

User.register = (userData, callback) => {
  const newUser = new User(userData);
  newUser.hash_password = bcrypt.hashSync(userData.password, 10);
  User.create(newUser, callback);
};

User.signIn = (authData, callback) => {
  User.findOne({ email: authData.email }, callback);
};

User.loginRequired = (req, res, next) => {
  if (req.user) {
    return next();
  }
  return res.status(401).json({ message: "Unauthorized user!" });
};

module.exports.User = User;
