const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const NewsSchema = new Schema({
  title: String,
  author: String,
  publishedDate: Date,
  content: String,
  img: String
});

NewsSchema.index({ title: "text", authors: "text", content: "text" });
const News = mongoose.model("news", NewsSchema);

module.exports = News;
