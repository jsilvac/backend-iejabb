const mongoose = require("mongoose");
const News = require("./news");
const User = require("./user");
const Event = require("./event");

const Schema = mongoose.Schema;

module.exports = {
  News,
  User,
  Event
};
