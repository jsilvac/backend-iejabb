const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const EventSchema = new Schema({
  title: String,
  publishedDate: Date,
  content: String,
  img: String,
  bannerAbstract: String
});
EventSchema.index({ title: "text", bannerAbstract: "text", content: "text" });
const Event = mongoose.model("events", EventSchema);

module.exports = Event;
