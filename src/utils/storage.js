const multer = require("multer");
const fs = require("fs");
const {
  LOCAL_NEWS_IMAGE_STORAGE,
  LOCAL_EVENTS_IMAGE_STORAGE,
  LOCAL_SYMBOLS_IMAGE_STORAGE,
  LOCAL_MEMBERS_IMAGE_STORAGE,
  LOCAL_BLOGS_IMAGE_STORAGE
} = require("./paths.js");

const newsStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, LOCAL_NEWS_IMAGE_STORAGE);
  },
  filename: (req, file, cb) => {
    cb(null, req.customName);
  }
});

const eventStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, LOCAL_EVENTS_IMAGE_STORAGE);
  },
  filename: (req, file, cb) => {
    cb(null, req.customName);
  }
});

const symbolsStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, LOCAL_SYMBOLS_IMAGE_STORAGE);
  },
  filename: (req, file, cb) => {
    cb(null, file.originalname);
  }
});

const membersStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, LOCAL_MEMBERS_IMAGE_STORAGE);
  },
  filename: (req, file, cb) => {
    cb(null, `${req.customName}-${file.originalname}`);
  }
});

const blogsStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, LOCAL_BLOGS_IMAGE_STORAGE);
  },
  filename: (req, file, cb) => {
    cb(null, `${req.customName}-${file.originalname}`);
  }
});

exports.deleteImage = (path, name) => {
  const filePath = `${path}/${name}`;
  try {
    fs.unlinkSync(filePath);
  } catch (e) {
    return false;
  }
  return true;
};

exports.newsStorage = newsStorage;
exports.eventStorage = eventStorage;
exports.symbolsStorage = symbolsStorage;
exports.membersStorage = membersStorage;
exports.blogsStorage = blogsStorage;
