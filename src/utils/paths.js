require("dotenv").config();

exports.BACKEND_BASE_PATH = process.env.PROJECT_LOCATION;
exports.BASE_PUBLIC_IMAGE_URL = `${process.env.SERVER_URL}:${process.env.SERVER_PORT}/public/images/`;
exports.LOCAL_IMAGE_STORAGE = "public/images";
exports.LOCAL_NEWS_IMAGE_STORAGE = `${exports.LOCAL_IMAGE_STORAGE}/news`;
exports.LOCAL_EVENTS_IMAGE_STORAGE = `${exports.LOCAL_IMAGE_STORAGE}/events`;
exports.LOCAL_SYMBOLS_IMAGE_STORAGE = `${exports.LOCAL_IMAGE_STORAGE}/symbols`;
exports.LOCAL_MEMBERS_IMAGE_STORAGE = `${exports.LOCAL_IMAGE_STORAGE}/members`;
exports.LOCAL_BLOGS_IMAGE_STORAGE = `${exports.LOCAL_IMAGE_STORAGE}/blogs`;
exports.LOCAL_SCHOOL_INFO = "data/schoolInfo.json";
