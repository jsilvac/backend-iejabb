// -- Library declaration--------
const express = require("express");

const schoolInfoRoutes = require("./schoolInfoRoutes.js");
const institutionalInfoRoutes = require("./institutionalInfoRoutes.js");
const contactRoutes = require("./contactRoutes.js");
const newsRoutes = require("./newsRoutes.js");
const eventRoutes = require("./eventRoutes.js");
const pqrsdRoutes = require("./pqrsdRoutes.js");
const adminCouncilRoutes = require("./adminCouncilRoutes.js");
const academicCouncilRoutes = require("./academicCouncilRoutes.js");
const schoolLifeCouncilRoutes = require("./schoolLifeCouncilRoutes.js");
const studentCouncilRoutes = require("./studentCouncilRoutes.js");
const professorsRoutes = require("./professorsRoutes.js");
const blogsRoutes = require("./blogsRoutes.js");
const rulesRoutes = require("./rulesRoutes.js");
const authRoutes = require("./authRoutes.js");

const router = express.Router();
/*
  Status Code
  200		Respuesta exitosa 
  201		Noticia Creada
  202		Aceptado pero sin respuesta 
  204		Petición con exito pero sin conteniedo 
  400		Necesita autenticación 
  405		Metodo no permitido
  500		Error interno de servidor 
  401		No autorizado 
  404		pagina no encontrada 
  405		metodo  no permitido.
*/

//-------------------------------------
router.get("/", (req, res) => {
  res.status(200);
  res.send("<h1>IETJABB backend</h1>");
  res.end();
});

schoolInfoRoutes(router);
institutionalInfoRoutes(router);
contactRoutes(router);
newsRoutes(router);
eventRoutes(router);
pqrsdRoutes(router);
adminCouncilRoutes(router);
academicCouncilRoutes(router);
schoolLifeCouncilRoutes(router);
studentCouncilRoutes(router);
professorsRoutes(router);
blogsRoutes(router);
authRoutes(router);
rulesRoutes(router);

//---------------Page not Found---------------
router.get("*", (req, res) => {
  res.status(404);
  res.send("Page not Found");
  res.end();
});

module.exports = router;
