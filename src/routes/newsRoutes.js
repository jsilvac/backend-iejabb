const multer = require("multer");

const authCheck = require("../middlewares/auth-check");
const { News } = require("../models");
const { newsStorage, deleteImage } = require("../utils/storage");
const {
  BASE_PUBLIC_IMAGE_URL,
  BACKEND_BASE_PATH,
  LOCAL_NEWS_IMAGE_STORAGE
} = require("../utils/paths");

const uploadNewsImage = multer({ storage: newsStorage }).single("img");

module.exports = router => {
  //Search text.
  router.get("/api/news/search/", async (req, res) => {
    /*Ejemplo
      /api/news/search/?text=jorge&initialDate=2020/01/01&finalDate=2020/03/29
      */
    var d1 = new Date(req.query.initialDate);
    var d2 = new Date(req.query.finalDate);
    var text = String(req.query.text);

    if (text !== "") {
      await News.find(
        { $text: { $search: text }, publishedDate: { $gte: d1, $lt: d2 } },
        function(err, data) {
          res.send(data);
          res.end();
        }
      ).sort({ publishedDate: -1 });
    } else {
      await News.find(
        {
          publishedDate: { $gte: d1, $lt: d2 }
        },
        function(err, data) {
          res.send(data);
          res.end();
        }
      ).sort({ publishedDate: -1 });
    }
  });

  // Get all news on Json format.
  router.get("/api/news/", async (req, res) => {
    const news = await News.find().sort({ publishedDate: -1 });
    res.send(news);
    res.end();
  });

  // Get a News with id
  router.get("/api/news/:id", async (req, res) => {
    const { id } = req.params;
    const news = await News.find({ _id: id });
    res.send(news);
    res.end();
  });

  // Create a new News
  router.post("/api/news/new", authCheck, async (req, res) => {
    const folderPath = BACKEND_BASE_PATH + LOCAL_NEWS_IMAGE_STORAGE;
    let newNews = await News(req.body);
    req.customName = String(newNews._id);
    uploadNewsImage(req, res, async function(err) {
      if (err instanceof multer.MulterError) {
        console.log("Multer error: " + err);
      } else if (err) {
        console.log("Unknown error: " + err);
      }

      Object.assign(newNews, {
        title: req.body.title,
        author: req.body.author,
        publishedDate: req.body.publishedDate,
        content: req.body.content,
        img: `${BASE_PUBLIC_IMAGE_URL}news/${req.customName}`
      });
      try {
        await newNews.save();
      } catch (e) {
        console.log(e);
        deleteImage(folderPath, req.customName);
        res.status(304);
        res.end();
        return;
      }

      res.send("<h1> Saved news </h1>");
      res.end();
    });
  });

  //Delete News with id
  router.delete("/api/news/delete/:id", authCheck, async (req, res) => {
    const { id } = req.params;
    const folderPath = BACKEND_BASE_PATH + LOCAL_NEWS_IMAGE_STORAGE;
    console.log(folderPath);
    const news = await News.deleteOne({ _id: id });
    deleteImage(folderPath, id);
    res.send(news);
    res.end();
  });

  // Update News
  router.put("/api/news/update/:id/", authCheck, (req, res) => {
    const { id } = req.params;
    req.customName = String(id);
    uploadNewsImage(req, res, async function(err) {
      if (err instanceof multer.MulterError) {
        console.log("Multer error: " + err);
      } else if (err) {
        console.log("Unknown error: " + err);
      }
      req.body.img = `${BASE_PUBLIC_IMAGE_URL}news/${req.customName}`;
      const news = await News.updateOne({ _id: id }, { $set: req.body });

      res.send(news);
      res.end();
    });
  });
};
