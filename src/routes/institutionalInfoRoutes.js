const fs = require("fs");
const multer = require("multer");

const authCheck = require("../middlewares/auth-check");
const { About } = require("../models");
const { LOCAL_SCHOOL_INFO, BASE_PUBLIC_IMAGE_URL } = require("../utils/paths");
const { symbolsStorage } = require("../utils/storage");

const uploadSymbolsImages = multer({
  storage: symbolsStorage
}).array("symbols", 2);

module.exports = router => {
  // Get about info
  router.get("/api/institutional-info", async (req, res) => {
    let rawdata = fs.readFileSync(LOCAL_SCHOOL_INFO);
    let institutionalInfo = JSON.parse(rawdata).institutionalInfo;
    res.send(institutionalInfo);
    res.end();
  });

  // Update info
  router.put("/api/institutional-info", authCheck, async (req, res) => {
    uploadSymbolsImages(req, res, async function(err) {
      if (err instanceof multer.MulterError) {
        console.log("Multer error: " + err);
      } else if (err) {
        console.log("Unknown error: " + err);
      }
      let rawdata = fs.readFileSync(LOCAL_SCHOOL_INFO);
      let schoolInfo = JSON.parse(rawdata);

      const contactProperties = Object.getOwnPropertyNames(
        schoolInfo.institutionalInfo
      );
      const data = JSON.parse(req.body.data);
      const dataProperties = Object.getOwnPropertyNames(data);

      schoolInfo.institutionalInfo.shieldUrl =
        BASE_PUBLIC_IMAGE_URL + "symbols/shield";
      schoolInfo.institutionalInfo.flagUrl =
        BASE_PUBLIC_IMAGE_URL + "symbols/flag";

      dataProperties.forEach(dataProperty => {
        if (schoolInfo.institutionalInfo.hasOwnProperty(dataProperty)) {
          schoolInfo.institutionalInfo[dataProperty] = data[dataProperty];
        }
      });
      const jsonContent = JSON.stringify(schoolInfo, null, 2);

      fs.writeFile(LOCAL_SCHOOL_INFO, jsonContent, "utf8", function(err) {
        if (err) {
          res
            .status(500)
            .send("An error occured while writing JSON Object to File.");
          return console.log(err);
        }
        res.send(schoolInfo);
        res.end();
      });
    });
  });
};
