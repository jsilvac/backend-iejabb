const multer = require("multer");

const authCheck = require("../middlewares/auth-check");
const { Event } = require("../models");
const { deleteImage, eventStorage } = require("../utils/storage");
const {
  BASE_PUBLIC_IMAGE_URL,
  BACKEND_BASE_PATH,
  LOCAL_EVENTS_IMAGE_STORAGE
} = require("../utils/paths");

const uploadEventsImage = multer({ storage: eventStorage }).single("img");

module.exports = router => {
  //Search text.
  router.get("/api/events/search/", async (req, res) => {
    /*Ejemplo
      /api/events/search/?text=jorge&initialDate=2020/01/01&finalDate=2020/03/29
      */
    var d1 = new Date(req.query.initialDate);
    var d2 = new Date(req.query.finalDate);
    var text = String(req.query.text);

    if (text !== "") {
      await Event.find(
        { $text: { $search: text }, publishedDate: { $gte: d1, $lt: d2 } },
        function(err, data) {
          res.send(data);
          res.end();
        }
      ).sort({ publishedDate: -1 });
    } else {
      await Event.find(
        {
          publishedDate: { $gte: d1, $lt: d2 }
        },
        function(err, data) {
          res.send(data);
          res.end();
        }
      ).sort({ publishedDate: -1 });
    }
  });

  // Get all events on Json format.
  router.get("/api/events/", async (req, res) => {
    const events = await Event.find().sort({ publishedDate: -1 });
    res.send(events);
    res.end();
  });

  // Get a News with id
  router.get("/api/events/:id", async (req, res) => {
    const { id } = req.params;
    const event = await Event.find({ _id: id });
    res.send(event);
    res.end();
  });

  // Create a new News
  router.post("/api/events/new", authCheck, async (req, res) => {
    const folderPath = BACKEND_BASE_PATH + LOCAL_EVENTS_IMAGE_STORAGE;
    let newEvent = await Event(req.body);
    req.customName = String(newEvent._id);
    uploadEventsImage(req, res, async function(err) {
      if (err instanceof multer.MulterError) {
        console.log("Multer error: " + err);
      } else if (err) {
        console.log("Unknown error: " + err);
      }

      Object.assign(newEvent, {
        title: req.body.title,
        publishedDate: req.body.publishedDate,
        content: req.body.content,
        img: `${BASE_PUBLIC_IMAGE_URL}events/${req.customName}`,
        bannerAbstract: req.body.bannerAbstract
      });
      try {
        await newEvent.save();
      } catch (e) {
        console.log(e);
        deleteImage(folderPath, req.customName);
        res.status(304);
        res.end();
        return;
      }

      res.send(newEvent);
      res.end();
    });
  });

  //Delete Event with id
  router.delete("/api/events/delete/:id", authCheck, async (req, res) => {
    const { id } = req.params;
    const folderPath = BACKEND_BASE_PATH + LOCAL_EVENTS_IMAGE_STORAGE;
    const event = await Event.deleteOne({ _id: id });
    deleteImage(folderPath, id);
    res.send(event);
    res.end();
  });

  // Update News
  router.put("/api/events/update/:id/", authCheck, (req, res) => {
    const { id } = req.params;
    req.customName = String(id);
    uploadEventsImage(req, res, async function(err) {
      if (err instanceof multer.MulterError) {
        console.log("Multer error: " + err);
      } else if (err) {
        console.log("Unknown error: " + err);
      }
      req.body.img = `${BASE_PUBLIC_IMAGE_URL}events/${req.customName}`;
      const event = await Event.updateOne({ _id: id }, { $set: req.body });

      res.send(event);
      res.end();
    });
  });
};
