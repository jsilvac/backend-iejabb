const fs = require("fs");

const authCheck = require("../middlewares/auth-check");
const { LOCAL_SCHOOL_INFO } = require("../utils/paths");

module.exports = router => {
  // Get information
  router.get("/api/rules", async (req, res) => {
    let rawdata = fs.readFileSync(LOCAL_SCHOOL_INFO);
    let rules = JSON.parse(rawdata).rules;
    res.send(rules);
    res.end();
  });

  // Update info
  router.put("/api/rules", authCheck, async (req, res) => {
    let rawdata = fs.readFileSync(LOCAL_SCHOOL_INFO);
    let schoolInfo = JSON.parse(rawdata);
    const rulesProperties = Object.getOwnPropertyNames(schoolInfo.rules);
    const reqBodyProperties = Object.getOwnPropertyNames(req.body);
    reqBodyProperties.forEach(reqBodyProperty => {
      if (schoolInfo.rules.hasOwnProperty(reqBodyProperty)) {
        schoolInfo.rules[reqBodyProperty] = req.body[reqBodyProperty];
      }
    });
    const jsonContent = JSON.stringify(schoolInfo, null, 2);

    fs.writeFile(LOCAL_SCHOOL_INFO, jsonContent, "utf8", function(err) {
      if (err) {
        res
          .status(500)
          .send("An error occured while writing JSON Object to File.");
        return console.log(err);
      }
      res.send(schoolInfo);
      res.end();
    });
  });
};
