const { Contact } = require("../models");
const fs = require("fs");

const authCheck = require("../middlewares/auth-check");
const { LOCAL_SCHOOL_INFO } = require("../utils/paths");

module.exports = router => {
  //-----------------Contacto page ----------------------
  router.get("/api/contact", async (req, res) => {
    let rawdata = fs.readFileSync(LOCAL_SCHOOL_INFO);
    let contactInfo = JSON.parse(rawdata).contact;
    res.send(contactInfo);
    res.end();
  });

  // Update Contact info
  router.put("/api/contact", authCheck, async (req, res) => {
    let rawdata = fs.readFileSync(LOCAL_SCHOOL_INFO);
    let schoolInfo = JSON.parse(rawdata);
    const contactProperties = Object.getOwnPropertyNames(schoolInfo.contact);
    const reqBodyProperties = Object.getOwnPropertyNames(req.body);

    reqBodyProperties.forEach(reqBodyProperty => {
      if (schoolInfo.contact.hasOwnProperty(reqBodyProperty)) {
        schoolInfo.contact[reqBodyProperty] = req.body[reqBodyProperty];
      }
    });

    const jsonContent = JSON.stringify(schoolInfo, null, 2);

    fs.writeFile(LOCAL_SCHOOL_INFO, jsonContent, "utf8", function(err) {
      if (err) {
        res
          .status(500)
          .send("An error occured while writing JSON Object to File.");
        return console.log(err);
      }
      res.send(schoolInfo);
      res.end();
    });
  });
};
