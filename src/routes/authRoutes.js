const { User } = require("../models/user");
const jwt = require("jsonwebtoken");
require("dotenv").config();

const authCheck = require("../middlewares/auth-check");
const secret = process.env.AUTH_SECRET;

module.exports = router => {
  router.post("/api/auth/register", authCheck, (req, res) => {
    if (req.user && req.user.username === "admin") {
      User.register(req.body, (err, user) => {
        if (err) {
          res.status(400).json(err.message);
          return;
        }
        res.json({
          message: `User with id ${user._id} successfully registered.`
        });
      });
    } else {
      res
        .status(401)
        .json({ message: `User with id ${req.user.id} is not authorized.` });
    }
  });

  router.post("/api/auth/login", (req, res) => {
    User.signIn(req.body, (err, user) => {
      if (err) {
        console.log(err);
      }
      if (!user) {
        res
          .status(401)
          .json({ message: "Authentication failed. User not found." });
      } else if (user) {
        if (!user.comparePassword(req.body.password)) {
          res
            .status(401)
            .json({ message: "Authentication failed. Wrong password." });
        } else {
          res.json({
            token: jwt.sign(
              {
                email: user.email,
                fullname: user.full_name,
                username: user.username,
                id: user._id
              },
              secret
            ),
            user: {
              email: user.email,
              fullname: user.full_name,
              username: user.username,
              id: user._id
            }
          });
        }
      }
    });
  });
};
