const fs = require("fs");

const { LOCAL_SCHOOL_INFO } = require("../utils/paths");

module.exports = router => {
  // Get information
  router.get("/api/school-info", async (req, res) => {
    let rawdata = fs.readFileSync(LOCAL_SCHOOL_INFO);
    let info = JSON.parse(rawdata);
    res.send(info);
    res.end();
  });
};
