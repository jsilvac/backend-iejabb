const fs = require("fs");
const multer = require("multer");

const authCheck = require("../middlewares/auth-check");
const {
  BACKEND_BASE_PATH,
  LOCAL_MEMBERS_IMAGE_STORAGE,
  LOCAL_SCHOOL_INFO,
  BASE_PUBLIC_IMAGE_URL
} = require("../utils/paths");
const { membersStorage, deleteImage } = require("../utils/storage");

const uploadMembersImages = multer({
  storage: membersStorage
}).array("images", 20);

module.exports = router => {
  // Get information
  router.get("/api/admin-council", async (req, res) => {
    let rawdata = fs.readFileSync(LOCAL_SCHOOL_INFO);
    let adminCouncil = JSON.parse(rawdata).institutionalGoverment.adminCouncil;
    res.send(adminCouncil);
    res.end();
  });

  // Update information
  router.put("/api/admin-council", authCheck, async (req, res) => {
    const date = new Date();
    req.customName = `admin-council-${date.getTime()}`;
    uploadMembersImages(req, res, async function(err) {
      if (err instanceof multer.MulterError) {
        console.log("Multer error: " + err);
      } else if (err) {
        console.log("Unknown error: " + err);
      }
      let rawdata = fs.readFileSync(LOCAL_SCHOOL_INFO);
      let schoolInfo = JSON.parse(rawdata);

      const contactProperties = Object.getOwnPropertyNames(
        schoolInfo.institutionalGoverment.adminCouncil
      );
      const data = JSON.parse(req.body.data);

      if (data.description) {
        schoolInfo.institutionalGoverment.adminCouncil.description =
          data.description;
      }

      data.createdMembers = data.createdMembers.map((member, i) => {
        let newMember = Object(member);
        newMember.pictureUrl = `${BASE_PUBLIC_IMAGE_URL}members/default_member`;
        newMember.id = `${req.customName}-${i}`;
        return newMember;
      });
      req.files.forEach((image, i) => {
        if (parseInt(image.originalname) === i) {
          data.createdMembers[i].id = `${req.customName}-${i}`;
          data.createdMembers[
            i
          ].pictureUrl = `${BASE_PUBLIC_IMAGE_URL}members/${image.filename}`;
        }
      });
      data.deletedMembers.forEach(memberId => {
        schoolInfo.institutionalGoverment.adminCouncil.members = schoolInfo.institutionalGoverment.adminCouncil.members.filter(
          member => member.id !== memberId
        );
        deleteImage(
          `${BACKEND_BASE_PATH}${LOCAL_MEMBERS_IMAGE_STORAGE}`,
          memberId
        );
      });
      schoolInfo.institutionalGoverment.adminCouncil.members.push(
        ...data.createdMembers
      );
      const jsonContent = JSON.stringify(schoolInfo, null, 2);

      fs.writeFile(LOCAL_SCHOOL_INFO, jsonContent, "utf8", function(err) {
        if (err) {
          res
            .status(500)
            .send("An error occured while writing JSON Object to File.");
          return console.log(err);
        }
        res.send(schoolInfo);
        res.end();
      });
    });
  });
};
