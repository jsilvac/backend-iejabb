#!/bin/bash

IMAGES_DIR="../public/images/*/"

TIME_SECONDS_OF_YEAR=31536000
TIME_SECONDS_OF_MONTH=2592000
TIME_SECONDS_OF_DAY=86400
TIME_SECONDS_OF_HOUR=3600
TIME_SECONDS_OF_MINUTE=60
TIME_SECONDS_OF_SECOND=1
MINIMUM_IMAGE_SIZE=200000

TIME_DIFF_TO_RESIZE=$(($TIME_SECONDS_OF_YEAR*1))
CURRENT_TIME=$(date +%s)

for dir in $IMAGES_DIR
do
  # remove the trailing "/"
  dir=${dir%*/}
  # print every directoty faound after the final "/"
  echo "=> searching old images in ${dir##*/} "
  for file in $dir/*; do
    [ -e "$file" ] || continue
    FILE_SIZE=$(stat --format="%s" $file) #ls -s $file | grep -Po "^[0-9]*")
    MOD_TIME=$(stat --format="%Y" $file)
    
    
    if [[ $FILE_SIZE -lt $MINUMUM_IMAGE_SIZE ]]
    then
      continue;
    fi

    if [ $(($CURRENT_TIME-$MOD_TIME)) -gt $TIME_DIFF_TO_RESIZE ]
    then
      convert $file -resize 400x400\> $file
      echo "resized  ${file##*/}"
    fi 
  done
done

